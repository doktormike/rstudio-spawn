# Rstudio Spawn

Spawns a new rstudio server session where a new user gets a login and a temporary directory. These sessions are not meant to last but rather to be closed down when you're done.

## Description

## Badges

## Visuals

## Installation

## Usage

## Support

## Roadmap

## Contributing

## Authors and acknowledgment

This repo is created based on the scripts provided by [Akraf](https://stackoverflow.com/users/3082472/akraf) in [Stackoverflow](https://stackoverflow.com/questions/22938305/run-multiple-instances-of-rstudio-in-a-web-browser).

## License

## Project status

