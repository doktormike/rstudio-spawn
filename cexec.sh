#!/usr/bin/env bash
# Execute a command in the container
set -ue

if [ "${1-}" = "--help" ]; then 
echo <<EOF
Usage: cexec command [args...]

Execute `command` in the container. This script starts the Singularity
container and executes the given command therein. The project root is mapped 
to the folder `/data` inside the container. Moreover, a temporary directory
is provided at `/tmp` that is removed after the end of the script.

EOF
exit 0
fi

thisdir="$(dirname "${BASH_SOURCE[0]}")"
container="rserver_200403.sif"

# Create a temporary directory
tmpdir="$(mktemp -d -t cexec-XXXXXXXX)"
# We delete this directory afterwards, so its important that $tmpdir
# really has the path to an empty, temporary dir, and nothing else!
# (for example empty string or home dir)
if [[ ! "$tmpdir" || ! -d "$tmpdir" ]]; then
  echo "Error: Could not create temp dir $tmpdir"
  exit 1
fi
# check if temp dir is empty (this might be superfluous, see
# https://codereview.stackexchange.com/questions/238439)
tmpcontent="$(ls -A "$tmpdir")"
if [ ! -z "$tmpcontent" ]; then
  echo "Error: Temp dir '$tmpdir' is not empty"
  exit 1
fi

# Start Singularity instance
instancename="$(basename "$tmpdir")"

# Maybe also superfluous (like above)
rundir="$(readlink -f "$thisdir/.run/$instancename")"
if [ -e "$rundir" ]; then
  echo "Error: Runtime directory '$rundir' exists already!" >&2
  exit 1
fi
mkdir -p "$rundir"

singularity instance start \
  --contain \
  -W "$tmpdir" \
  -H "$thisdir:/data" \
  -B "$rundir:/data/.rstudio" \
  -B "$thisdir/.rstudio/monitored/user-settings:/data/.rstudio/monitored/user-settings" \
  "$container" \
  "$instancename"

# Delete the temporary directory after the end of the script
trap "singularity instance stop '$instancename'; rm -rf '$tmpdir'; rm -rf '$rundir'" EXIT
singularity exec \
  --pwd "/data" \
  "instance://$instancename" \
  "$@"
