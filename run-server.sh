#!/bin/env bash
set -ue

#thisdir="$(dirname "${BASH_SOURCE[0]}")"
thisdir="$(pwd)/rstudiodata"
uid="$(id -u)"

# Return 0 if the port $1 is free, else return 1
is_port_free(){
  port="$1"
  set +e
  netstat -an | 
    grep --color=none "^tcp.*LISTEN\s*$" | \
    awk '{gsub("^.*:","",$4);print $4}' | \
    grep -q "^$port\$"
  r="$?"
  set -e
  if [ "$r" = 0 ]; then return 1; else return 0; fi
}

# Find a free port
find_free_port(){
    local lower_port="$1"
    local upper_port="$2"
    for ((port=lower_port; port <= upper_port; port++)); do
      if is_port_free "$port"; then r=free; else r=used; fi
      if [ "$r" = "used" -a "$port" = "$upper_port" ]; then
        echo "Ports $lower_port to $upper_port are all in use" >&2
        exit 1
      fi
      if [ "$r" = "free" ]; then break; fi
    done
    echo $port
}

port=$(find_free_port 8100 8200)

echo "Access RStudio Server on http://localhost:$port" >&2

#"$thisdir/cexec" \
#    rserver \
#    --www-address 127.0.0.1 \
#    --www-port $port

docker run -d -v $thisdir:/home/rstudio --env USERID=$UID --env PASSWORD=1q2w3e4r5t -p $port:8787 bammm

